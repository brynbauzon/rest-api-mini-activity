const express = require('express');
const router = express.Router();

const productController = require('../controllers/productController');

//Route to add a product
router.post('/', (req, res)=>{

  productController.createProduct(req.body).then(resultFromController => res.send(resultFromController))

});


//Route to get all product

router.get('/', (req, res)=>{
 productController.getAllProducts().then(resultFromController => res.send(resultFromController));

});


//Route to view a specific product
router.get(':/id', (req, res)=>{

 taskController.getProduct(req.params.id).then(resultFromController => res.send(resultFromController));

});

//Route to delete a specific product
router.delete(':/id', (req, res)=>{

taskController.deleteProduct(req.params.id).
   then(resultFromController => res.send(resultFromController));

});

//Use module.exports to export the router object to use in the app.js
module.exports = router;