//Product Model

const Product = require('../models/product');


//Create Product Controller

module.exports.createProduct = (requestBody) =>{

  let newProduct = new Product({

  		name: requestBody.name,
  		price: requestBody.price
  });

  	return newProduct.save().then((result, error)=>{

  		if(error){

  			console.log(error);
  		}else{

  			return result
  		}

  	});
};


//Get all Product Controller

module.exports.getAllProducts = () =>{

return Task.find({}).then((result) =>{

  return result;

  });

};


//Get single product
module.exports.getProduct = (productId) => {

   
 return Task.findById(taskId).then((result, error) =>{
      if(error){

        console.log(error);
        return false;

      }else{

        return result;

      }


  });
};

//Delete specific product

module.exports.deleteProduct = (productId) => {

 return Task.findByIdAndRemove(productId).then((result, error) => {

      if(error){

         console.log(error);
         return false

      }else{

       return result;
      }

 });
};